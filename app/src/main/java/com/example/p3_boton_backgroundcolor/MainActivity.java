package com.example.p3_boton_backgroundcolor;

import androidx.appcompat.app.AppCompatActivity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.btn_Red).setOnClickListener(this);
        findViewById(R.id.btn_Blue).setOnClickListener(this);
        findViewById(R.id.btn_Green).setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        int color;

        View Container = v.getRootView();

        switch (v.getId()){
            case R.id.btn_Red:
                color = Color.parseColor("#FF0000");
                break;
            case R.id.btn_Blue:
                color = Color.parseColor("#0000FF");
                break;
            case R.id.btn_Green:
                color = Color.parseColor("#00FF00");
                break;
            default:
                color = Color.WHITE;
        }
        Container.setBackgroundColor(color);
    }
}
